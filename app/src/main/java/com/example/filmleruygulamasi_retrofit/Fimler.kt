package com.example.filmleruygulamasi_retrofit

import java.io.Serializable

data class Fimler(var film_id:Int,
                  val film_ad:String,
                  var film_yil:Int,
                  var film_resim:String, var kategori:Kategoriler,var yonetmen:Yonetmenler):Serializable {
}