package com.example.filmleruygulamasi_retrofit

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.filmleruygulamasi_retrofit.R

import com.example.filmleruygulamasi_retrofit.databinding.ActivityDetayBinding

class DetayActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetayBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityDetayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val film = intent.getSerializableExtra("filmNesne") as Fimler
        binding.textViewFilmAd.text = film.film_ad
        binding.textViewFilmYil.text = film.film_yil.toString()
        binding.textViewYonetmen.text = film.yonetmen.yonetmen_ad

        binding.imageViewResim.setImageResource(this.resources.getIdentifier(film.film_resim,"drawable",this.packageName))
    }
}